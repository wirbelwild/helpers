# Changes in Bit&Black Helpers v2.1

## 2.1.0 2024-06-18

### Added 

-   The method `StringHelper::mbStrRev` has been added.