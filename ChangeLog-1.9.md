# Changes in Bit&Black Helpers v1.9

## 1.9.0 2022-01-31

### Changed 

-   Refactored method `ArrayHelper::recurse` so the callable may receive an array key as second parameter if existing.
-   Improved behaviour of `StringHelper::stringToNumber`, `StringHelper::stringToInt` and `StringHelper::stringToFloat`.