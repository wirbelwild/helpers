# Changes in Bit&Black Helpers 1.5

## 1.5.2 2021-01-20

### Fixed 

-   Fixed bug in `RequestHelper::sortUploadFiles` that couldn't handle files with strings as keys.

## 1.5.1 2020-12-10

### Fixed

-   Fixed return type in `XMLHelper::hasGetOr`.

## 1.5.0 2020-11-18

### Added 

-   Added `mbUcFirst` to covert the first character to upper case.